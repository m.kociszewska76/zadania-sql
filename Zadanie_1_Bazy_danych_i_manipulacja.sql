-- Zadanie 1 Bazy danych i manipulacja
-- ad 1
CREATE DATABASE Pracownicy;

-- ad 2
CREATE TABLE Pracownicy.Pracownicy (
id int PRIMARY KEY,
imie varchar(100) NOT NULL,
nazwisko varchar(100) NOT NULL,
wiek int NOT NULL,
kurs TEXT NOT NULL
);

-- ad 3
INSERT INTO Pracownicy.Pracownicy
(id, imie, nazwisko, wiek, kurs)
VALUES 
		(1, 'Anna', 'Nowak', 34, 'DS.'),
        (2, 'Roman', 'Kowalski', 42, 'DS.'),
        (3, 'Tomasz', 'Wiśniewski', 33, 'DS.'),
        (4, 'Agata', 'Wójcik', 43, 'DS.'),
        (5, 'Elżbieta', 'Kowalczyk', 28, 'Java'),
        (6, 'Przemysław', 'Kamiński', 34, 'Java'),
        (7, 'Robert', 'Lewandowski', 35, 'Java'),
        (8, 'Radosław', 'Zieliński', 38, 'Java'),
        (9, 'Anna', 'Wozniak', 26, 'Java'),
        (10, 'Robert', 'Szymański', 34, 'Java'),
        (11, 'Radosław', 'Dąbrowski', 35, 'UX'),
        (12, 'Robert', 'Kozłowski', 38, 'UX'),
        (13, 'Joanna', 'Mazur', 26, 'UX'),
        (14, 'Radosław', 'Jankowski', 27, 'UX'),
        (15, 'Patryk', 'Lewandowski', 28, 'Tester'),
        (16, 'Patryk', 'Zieliński', 28, 'Tester'),
        (17, 'Andrzej', 'Wozniak', 31, 'Tester'),
        (18, 'Andrzej', 'Lewandowski', 30, 'Tester'),
        (19, 'Roman', 'Zieliński', 39, 'Tester'),
        (20, 'Ewa', 'Wozniak', 31, 'Tester');
        
-- ad 4
-- wyswietlam listę pracowników starszych niz 30 lat, ale nie uwzgledniam 30-letnich, robie >30, a nie >=30
SELECT id, imie, nazwisko, wiek, kurs FROM Pracownicy.Pracownicy WHERE wiek >30;

-- ad 5
-- podobnie jak w pkt 4, uwzgledniam tylko pracownikow majacych <30, a nie <=30
SELECT id, imie, nazwisko, wiek, kurs FROM Pracownicy.Pracownicy WHERE wiek <30;

-- 6 
SELECT * 
FROM Pracownicy.Pracownicy
WHERE nazwisko LIKE "K%ki";

-- 7
ALTER TABLE Pracownicy.Pracownicy RENAME COLUMN id TO nr;

-- ad 8
SELECT * 
FROM Pracownicy.Pracownicy 
WHERE id IS NULL OR imie IS NULL OR nazwisko IS NULL OR wiek IS NULL OR kurs IS NULL;

-- ad 9
SELECT * FROM Pracownicy.Pracownicy WHERE kurs = 'Java';




