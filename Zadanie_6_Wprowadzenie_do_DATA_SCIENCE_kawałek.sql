-- Zadanie 6 Wprowadzenie do DATA SCIENCE

-- ad 1
CREATE DATABASE Moje_Zainteresowania;

-- ad 2
CREATE TABLE Moje_Zainteresowania.Zainteresowania (
id INTEGER PRIMARY KEY,
nazwa TEXT NOT NULL,
opis TEXT NOT NULL,
data_realizacji DATE NOT NULL
);

-- ad 3
INSERT INTO Moje_Zainteresowania.Zainteresowania (nazwa, opis)
VALUES
	('Rower', 'Szybka jazda w terenie'),
    ('Ksiazki', 'Podroznicze'),
    ('Majsterkowanie', 'Tworzenie wynalazkow'),
    ('Programowanie', 'Programowanie gier komputerowych'),
    ('Nordic walking', 'Aktywne zajecia w terenie');
    






