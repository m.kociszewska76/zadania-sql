-- Zadanie 5 Bazy danych i manipulacja
-- ad 1
CREATE DATABASE Pracownicy;

-- ad 2
CREATE TABLE Pracownicy.Pracownicy (
id int PRIMARY KEY,
imie varchar(100) NULL,
nazwisko varchar(100) NULL,
wiek int NULL,
kurs TEXT NULL
);

-- ad 3
INSERT INTO Pracownicy.Pracownicy
(id, imie, nazwisko, wiek, kurs)
VALUES 
		(1, 'Anna', 'Nowak', 34, 'DS.'),
        (2, 'Roman', 'Kowalski', 42, 'DS.'),
        (3, 'Tomasz', 'Wiśniewski', 33, 'DS.'),
        (4, 'Anna', NULL, 43, 'DS.'),
        (5, 'Elżbieta', 'Kowalczyk', NULL, 'Tester'),
        (6, 'Anna', 'Kowalczyk', NULL, 'Java'),
        (7, 'Robert', 'Kowalczyk', NULL, 'Java'),
        (8, 'Radosław', 'Zieliński', NULL, 'Java'),
        (9, 'Robert', 'Wozniak', NULL, 'Java'),
        (10, 'Robert', 'Szymański', 34, 'Tester'),
        (11, 'Radosław', 'Dąbrowski', 35, NULL),
        (12, 'Robert', 'Kozłowski', NULL, 'UX'),
        (13, 'Joanna', 'Mazur', 26, 'UX'),
        (14, 'Radosław', 'Jankowski', 27, 'UX'),
        (15, 'Patryk', 'Lewandowski', 28, NULL),
        (16, NULL, 'Zieliński', 28, NULL),
        (17, 'Andrzej', 'Wozniak', 31, NULL),
        (18, 'Andrzej', 'Lewandowski', 30, NULL),
        (19, 'Roman', 'Kowalczyk', 39, NULL),
        (20, 'Ewa', 'Wozniak', 31, NULL);
        
-- ad 4
SELECT DISTINCT imie
FROM Pracownicy.Pracownicy;

-- ad 5
SELECT DISTINCT nazwisko
FROM Pracownicy.Pracownicy;

-- ad 6 
SELECT kurs
FROM Pracownicy.Pracownicy
WHERE nazwisko = 'Kowalczyk';

-- ad 7 
SELECT * 
FROM Pracownicy.Pracownicy
WHERE wiek IS NULL;

-- ad 8 
SELECT wiek
FROM Pracownicy.Pracownicy
WHERE imie = 'Patryk';

-- ad 9
ALTER TABLE Pracownicy.Pracownicy RENAME Pracownicy.Mentorzy;
