-- Zadanie 2 Bazy danych i manipulacja
-- ad 1
CREATE DATABASE Pracownicy;

-- ad 2
CREATE TABLE Pracownicy.Pracownicy (
id int PRIMARY KEY,
imie varchar(100) NULL,
nazwisko varchar(100) NULL,
wiek int NULL,
kurs TEXT NULL
);

-- ad 3
INSERT INTO Pracownicy.Pracownicy
(id, imie, nazwisko, wiek, kurs)
VALUES 
		(1, 'Anna', 'Nowak', 34, 'DS.'),
        (2, 'Roman', 'Kowalski', 42, 'DS.'),
        (3, 'Tomasz', 'Wiśniewski', 33, 'DS.'),
        (4, 'Agata', 'Wójcik', 43, 'DS.'),
        (5, 'Elżbieta', 'Kowalczyk', 28, 'Java'),
        (6, 'Przemysław', NULL, 34, 'Java'),
        (7, 'Robert', NULL, 35, 'Java'),
        (8, 'Radosław', 'Zieliński', 38, 'Java'),
        (9, NULL, 'Wozniak', 26, 'Java'),
        (10, 'Robert', 'Szymański', 34, 'Java'),
        (11, 'Radosław', 'Dąbrowski', 35, NULL),
        (12, 'Robert', 'Kozłowski', NULL, 'UX'),
        (13, 'Joanna', 'Mazur', 26, 'UX'),
        (14, 'Radosław', 'Jankowski', 27, 'UX'),
        (15, 'Patryk', 'Lewandowski', 28, 'Tester'),
        (16, NULL, 'Zieliński', 28, 'Tester'),
        (17, 'Andrzej', 'Wozniak', 31, 'Tester'),
        (18, 'Andrzej', 'Lewandowski', 30, 'Tester'),
        (19, 'Roman', NULL, 39, 'Tester'),
        (20, 'Ewa', 'Wozniak', 31, 'Tester');
        
-- ad 4
SELECT * 
FROM Pracownicy.Pracownicy
WHERE wiek = 28;

-- ad 5
SELECT * 
FROM Pracownicy.Pracownicy
WHERE wiek <= 30;

-- ad 6 
SELECT * 
FROM Pracownicy.Pracownicy
WHERE nazwisko LIKE "%ski%";

-- ad 7
SELECT * 
FROM Pracownicy.Pracownicy
WHERE id = 1 OR id = 4 OR id = 7 OR id = 18 OR id = 20;

-- ad 8
SELECT * 
FROM Pracownicy.Pracownicy
WHERE  id IS NOT NULL AND imie IS NOT NULL AND nazwisko IS NOT NULL AND wiek IS NOT NULL AND kurs IS NOT NULL;

-- ad 9
SELECT * 
FROM Pracownicy.Pracownicy
WHERE kurs != 'DS.';