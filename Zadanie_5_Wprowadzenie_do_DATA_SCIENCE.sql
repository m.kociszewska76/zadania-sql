-- Zadanie 5 Wprowadzenie do DATA SCIENCE

-- ad 1
CREATE DATABASE Lista_Pracownikow;

-- ad 2
CREATE TABLE Lista_Pracownikow.Pracownicy (
id INTEGER PRIMARY KEY,
imie TEXT NOT NULL,
nazwisko TEXT NOT NULL,
stanowisko TEXT NOT NULL,
dzial TEXT NOT NULL
);

-- ad 3
INSERT INTO Lista_Pracownikow.Pracownicy
(id, imie, nazwisko, stanowisko, dzial)
VALUES 
		(1, 'Dominik', 'Nowak', 'Programista', 'IT'),
        (2, 'Agnieszka', 'Rutkowska', 'Inżynier', 'Produkcja'),
        (3, 'Karolina', 'Klepacz', 'Ksiegowa', 'Ksiegowosc'),
        (4, 'Malgorzata', 'Kuszewska', 'Scrum_Master', 'IT'),
        (5, 'Paweł', 'Dominiak', 'Data_Scientist', 'IT');

-- ad 4
SELECT * FROM  Lista_Pracownikow.Pracownicy;

-- ad 5
ALTER TABLE Lista_Pracownikow.Pracownicy
ADD COLUMN data_zatrudnienia DATE;

-- ad 6
INSERT INTO Lista_Pracownikow.Pracownicy
(id, imie, nazwisko, stanowisko, dzial, data_zatrudnienia)
VALUE (6, 'Sylwia', 'Wlodarczyk', 'Kadrowa', 'Ksiegowosc', '2020-08-31');

-- ad 7
SELECT * FROM  Lista_Pracownikow.Pracownicy;
-- lub
SELECT id, imie, nazwisko, stanowisko, dzial, data_zatrudnienia FROM Lista_Pracownikow.Pracownicy;
-- ad 8
UPDATE Lista_Pracownikow.Pracownicy
SET data_zatrudnienia = '2019-07-31'
WHERE id = 1;

UPDATE Lista_Pracownikow.Pracownicy
SET data_zatrudnienia = '2017-05-15'
WHERE id = 2;

UPDATE Lista_Pracownikow.Pracownicy
SET data_zatrudnienia = '2015-09-01'
WHERE id = 3;

UPDATE Lista_Pracownikow.Pracownicy
SET data_zatrudnienia = '2020-01-01'
WHERE id = 4;

UPDATE Lista_Pracownikow.Pracownicy
SET data_zatrudnienia = '2019-02-01'
WHERE id = 5;

-- ad 9
-- wyswietlam kolumne data_zatrudnienia razem z id
SELECT id, data_zatrudnienia FROM Lista_Pracownikow.Pracownicy;
-- lub wyswietlam sama zmieniona kolumne zatrudnienia
SELECT data_zatrudnienia FROM Lista_Pracownikow.Pracownicy;

