-- Zadanie 2 Wprowadzenie do DATA SCIENCE
-- ad 1
CREATE DATABASE baza1;
-- ad 2
CREATE TABLE baza1.Biblioteczka (
id int,
tytul varchar(100),
datazakupu varchar(100)
);
-- ad 3
INSERT INTO Biblioteczka
(id, tytul, datazakupu)
VALUES (1, 'Transerfing rzeczywistosci', '2019-09-22');
-- ad 4
SELECT * FROM Biblioteczka; 
-- ad 5
INSERT INTO Biblioteczka
(id, tytul, datazakupu)
VALUES (2, 'Kod Leonardo da Vinci', '2020-08-12');
INSERT INTO Biblioteczka
(id, tytul, datazakupu)
VALUES (3, 'Nastepne sto lat', '2015-07-14');
-- ad 6
SET SQL_SAFE_UPDATES=0;
UPDATE Biblioteczka
SET tytul = "Skandalisci PRL"
WHERE id = 3;
-- ad 7
ALTER TABLE Biblioteczka
ADD COLUMN autor TEXT;
-- ad 8
SET SQL_SAFE_UPDATES=0;
UPDATE Biblioteczka
SET autor = "Vadim Zeland"
WHERE id = 1;
UPDATE Biblioteczka
SET autor = "Dan Brown"
WHERE id = 2;
UPDATE Biblioteczka
SET autor = "Slawomir Koper"
WHERE id = 3;
-- ad 9
DELETE FROM Biblioteczka
WHERE id = 1;
DELETE FROM Biblioteczka
WHERE id = 2;
-- ad 10
SELECT autor FROM Biblioteczka;


