-- Zadanie 1 Agregaty - Podstawowa składnia jezyka SQL

CREATE DATABASE aggregates;
CREATE TABLE aggregates.batman(
  id INTEGER PRIMARY KEY NOT NULL,
  first_name TEXT,
  last_name TEXT,
  sex CHAR(1),
  age INTEGER,
  price DOUBLE,
  start_date DATE,
  gift TEXT
);

INSERT INTO aggregates.batman VALUES (1, 'Alicja', 'Rogal', 'F', 16, 100.25,'2020-01-02', 'rower' );
INSERT INTO aggregates.batman VALUES (2, 'Iwona', 'Kowalska', 'F', 33, 56.58, '2020-01-03', 'komputer');
INSERT INTO aggregates.batman VALUES (3, 'Igor', 'Kowalski', 'M', 50, 68.00, '2020-01-04', 'karty');
INSERT INTO aggregates.batman VALUES (4, 'Kamil', 'Juszczak', 'M', 50, 40.87, '2020-01-05', 'piłka'); 
INSERT INTO aggregates.batman VALUES (5, 'Konrad', 'Kowal', 'M', 18, 32.63, '2020-01-06', 'herbata' );
INSERT INTO aggregates.batman VALUES (6, 'Iwona', 'Feniks', 'F', 35, 78.98, '2020-01-07', 'okno' );
INSERT INTO aggregates.batman VALUES (7, 'Robert', 'Lew', 'M', 40, 120.32, '2020-01-08', 'drzwi');
INSERT INTO aggregates.batman VALUES (8, 'Tomasz', 'Nowak', 'M', 60, 150.00, '2020-01-09', 'korona');
INSERT INTO aggregates.batman VALUES (9, 'Aldona', 'Buk', NULL, NULL, 121.25, '2020-01-10', 'wycieczka');

-- ad a
SELECT Count(id) 
FROM aggregates.batman;

-- ad b
SELECT COUNT(age)
FROM aggregates.batman;

-- ad c
SELECT COUNT(sex)
FROM aggregates.batman
WHERE sex = 'M' AND age>40;

-- ad d
SELECT SUM(price)
FROM aggregates.batman;

-- ad e
SELECT SUM(age)
FROM aggregates.batman
WHERE sex = 'F';

-- ad f
SELECT SUM(price)
FROM aggregates.batman
WHERE gift = 'komputer' OR gift = 'okno';

-- ad g
SELECT MAX(price)
FROM aggregates.batman;

-- ad h
-- tak pokazuje najpozniejsza date, ale tez wszystkie pozostale daty
SELECT start_date
FROM aggregates.batman
ORDER BY start_date DESC;

-- można tez zrobic tak i wtedy pokazuje tylko te date najpozniejsza bez pozostalych dat
SELECT MIN(start_date)
FROM aggregates.batman;

-- ad i
SELECT last_name
FROM aggregates.batman
ORDER BY last_name DESC
LIMIT 1;

-- ad j
SELECT MIN(price)
FROM aggregates.batman;

-- ad k
SELECT start_date
FROM aggregates.batman
ORDER BY start_date ASC
LIMIT 1;

-- ad l
SELECT last_name
FROM aggregates.batman
ORDER BY last_name ASC
LIMIT 1;

-- ad m 
SELECT MAX(price) AS max, MIN(price) AS min
FROM aggregates.batman;

-- ad n
SELECT MAX(price) - MIN(price)
FROM aggregates.batman;

-- ad o
SELECT AVG(age)
FROM aggregates.batman;

-- ad p 
-- srednia wieku bez zaokraglenia
SELECT SUM(age)/COUNT(id)
FROM aggregates.batman;

-- srednia wieku zaokraglona do liczby calkowitej
SELECT ROUND (SUM(age)/COUNT(id))
FROM aggregates.batman;

-- ad q
-- sredni wiek dla mezczyzn
SELECT AVG(age)
FROM aggregates.batman
WHERE sex = 'M';
-- sredni wiek dla kobiet
SELECT AVG(age)
FROM aggregates.batman
WHERE sex = 'F';