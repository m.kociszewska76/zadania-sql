-- ad 1
CREATE DATABASE Moje_Zainteresowania;

-- ad 2
CREATE TABLE Moje_Zainteresowania.Zainteresowania (
id INTEGER NULL,
nazwa TEXT NULL,
opis TEXT NULL,
data_realizacji DATE NULL
);

-- ad 3
INSERT INTO Moje_Zainteresowania.Zainteresowania (nazwa, opis)
VALUES
	('Rower', 'Szybka jazda w terenie'),
    ('Ksiazki', 'Podroznicze'),
    ('Majsterkowanie', 'Tworzenie wynalazkow'),
    ('Programowanie', 'Programowanie gier komputerowych'),
    ('Nordic walking', 'Aktywne zajecia w terenie');
    
-- ad 4
-- Dane wyświetlają się. W Kolumnie id i data_realizacji sa wartosci NULL
SELECT * FROM Moje_Zainteresowania.Zainteresowania;
    
-- ad 5
INSERT INTO Moje_Zainteresowania.Zainteresowania (id, nazwa, opis, data_realizacji)
VALUE (6, 'Fotografia', 'Robienie zdjec natury', '2022-10-31');

-- ad 6
SET SQL_SAFE_UPDATES=0;
UPDATE Moje_Zainteresowania.Zainteresowania
SET data_realizacji = '2022-12-31'
WHERE nazwa = 'Programowanie';
UPDATE Moje_Zainteresowania.Zainteresowania
SET id = 4
WHERE nazwa = 'Programowanie';

-- ad 7
SELECT * FROM Moje_Zainteresowania.Zainteresowania;

-- ad 8
DELETE FROM Moje_Zainteresowania.Zainteresowania
WHERE id IS NULL OR nazwa IS NULL OR opis IS NULL OR data_realizacji IS NULL;

-- ad 9
SELECT * FROM Moje_Zainteresowania.Zainteresowania;