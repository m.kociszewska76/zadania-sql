-- zadanie 3 Agregaty Podstawowa składnia języka SQL

-- ad a
USE aggregates;

SELECT start_date,
EXTRACT(YEAR FROM start_date) AS ROK, EXTRACT(MONTH FROM start_date) AS MIESIAC, EXTRACT(DAY FROM start_date) AS DZIEN 
FROM batman;

-- ad b

SELECT start_date,
DATE_ADD(start_date, INTERVAL 3 DAY) AS final_date
FROM batman;

-- ad c

SELECT CURDATE() 

-- ad d

SELECT MONTHNAME('2022-10-18')

-- ad e - wszystko wyświetlam osobno, a nie wiem, jak to połączyć, żeby wyświrtliło mi się wszystko razem, czyli łącznie 5 kolumn

-- wyswietlam start_date
SELECT start_date
FROM batman;

-- wyświetlam numer tygodnia

SELECT WEEKDAY(start_date) AS numer_tygodnia
FROM batman;

-- wyświetlam nazwę miesiąca
SELECT MONTHNAME (start_date) AS nazwa_miesiaca
FROM batman;

-- wyświetlam kwartał
SELECT QUARTER(start_date) AS kwartal
FROM batman;

-- wyświetlam numer dnia w roku
SELECT DAYOFYEAR(start_date) AS dzien_w_roku
FROM batman;

-- wyświetlam wszystko razem
SELECT start_date, 
WEEKDAY(start_date) AS numer_tygodnia, 
MONTHNAME (start_date) AS nazwa_miesiaca, 
QUARTER(start_date) AS kwartal, 
DAYOFYEAR(start_date) AS dzien_w_roku
FROM batman
