-- Projekt 1 - SQL

-- ad 1 - Stwórz bazę "Sklep odzieżowy"

CREATE DATABASE sklep_odziezowy;

-- ad 2 - Utwórz tabelę "Producenci" z kolumnami

USE sklep_odziezowy;
CREATE TABLE producenci (
id_producenta Varchar(10) PRIMARY KEY,
nazwa_producenta Varchar(40) NOT NULL,
adres_producenta Varchar(40) NOT NULL,
nip_producenta Varchar(20) NOT NULL,
data_podpisania_umowy Date NOT NULL
);

-- ad 3 - Utwórz tabelę "Produkty" z kolumnami

USE sklep_odziezowy;
CREATE TABLE produkty (
id_produktu Varchar(10) PRIMARY KEY,
id_producenta Varchar(40) NOT NULL,
nazwa_produktu Varchar(40) NOT NULL,
opis_produktu Varchar(100) NOT NULL,
cena_netto_zakupu Decimal(10,2) NOT NULL, 
cena_brutto_zakupu Decimal(10,2) NOT NULL,
cena_netto_sprzedazy Decimal(10,2) NOT NULL,
cena_brutto_sprzedazy Decimal(10,2) NOT NULL, 
procent_VAT_sprzedazy Varchar(10) DEFAULT NULL
);

-- ad 4 - Utwórz tabelę "Zamówienia" z kolumnami

USE sklep_odziezowy;

CREATE TABLE zamowienia (
id_zamowienia Varchar(10) PRIMARY KEY,
id_klienta Varchar(10) NOT NULL,
id_produktu Varchar(10) NOT NULL,
data_zamowienia Date NOT NULL
);

-- ad 5 - Utwórz tabelę "Klienci" z kolumnami

USE sklep_odziezowy;

CREATE TABLE klienci (
id_klienta Varchar(10) PRIMARY KEY,
id_zamowienia Varchar(10) NOT NULL,
imie Text NOT NULL,
nazwisko Text NOT NULL,
adres Text NOT NULL
);

-- ad 6 - Połącz tabele ze sobą za pomocą kluczy obcych

-- Produkty-Producenci

USE sklep_odziezowy;

ALTER TABLE produkty 
ADD CONSTRAINT id_producenta_fk 
FOREIGN KEY (id_producenta) 
REFERENCES producenci (id_producenta);

-- Zamówienia-Produkty

USE sklep_odziezowy;

ALTER TABLE zamowienia
ADD CONSTRAINT id_produktu_fk 
FOREIGN KEY (id_produktu) 
REFERENCES produkty (id_produktu);

-- Zamówienia-Klienci

USE sklep_odziezowy;

ALTER TABLE klienci
ADD CONSTRAINT id_zamowienia_fk 
FOREIGN KEY (id_zamowienia) 
REFERENCES zamowienia (id_zamowienia);

-- ad 7 - Każdą tabelę uzupełnij danymi wg:

USE sklep_odziezowy;

-- Tabela "Producenci" - 4 pozycje

INSERT INTO producenci 
VALUES ('P1', 'Bluzki sp. z o.o.', 'ul. Nowa 9, Zamość', '728-999-10-10', '2020-10-09'),
	   ('P2', 'Spodnie sp.j.', 'ul. Kwiatowa 22, Warszawa', '798-980-09-76', '2019-07-01'),
       ('P3', 'T-Shirts s.c.', 'ul. Akacjowa 34/23, Toruń', '734-875-75-23', '2018-04-17'),
       ('P4', 'Bielizna sp.j.', 'ul. Sosnowa 35, Katowice', '765-098-76-34', '2015-12-01');

-- Tabela "Produkty" - 20 pozycji

USE sklep_odziezowy;

INSERT INTO produkty
VALUES ('PR1', 'P1', 'Bluzka', 'Bluzka wizytowa', 100.00, 123.00, 120.00, 147.60, '23%'),
	   ('PR2', 'P1', 'Bluzeczka', 'Bluzka dla dziewczynki', 50.00, 61.50, 75.00, 92.25, '23%'),
       ('PR3', 'P2', 'Spodnie', 'Męskie spodnie', 150.00, 184.50, 190.00, 233.70, '23%'),
       ('PR4', 'P3', 'T-shirt', 'Polówka', 60.00, 73.80, 95.00, 116.85, '23%'),
       ('PR5', 'P2', 'Spodenki', 'Dziecięce spodnie', 115.00, 141.45, 155.00, 190.65, '23%'),
       ('PR6', 'P3', 'Koszulka', 'Koszulka dziewczęca', 67.00, 82.41, 112.00, 137.76, '23%'),
       ('PR7', 'P4', 'Majtki', 'Majtki dla kobiet', 35.00, 43.05, 50.00, 61.50, '23%'),
       ('PR8', 'P4', 'Majteczki', 'Majtki dla dziewczynek', 27.00, 33.21, 35.00, 43.05, '23%'),
       ('PR9', 'P4', 'Bokserki', 'Bokserki dla mężczyzn', 32.00, 39.36, 43.00, 52.89, '23%'),
       ('PR10', 'P4', 'Skarpety', 'Skarpety dla mężczyzn', 23.00, 28.29, 29.00, 35.67, '23%'),
       ('PR11', 'P4', 'Skarpetki', 'Skarpetki dla kobiet', 17.00, 20.91, 24.00, 29.52, '23%'),
       ('PR12', 'P4', 'Stopki', 'Stopki dla kobiet', 15.00, 18.45, 20.00, 24.60, '23%'),
       ('PR13', 'P4', 'Stopeczki', 'Stopki dla dziewczynek', 12.00, 14.76, 15.00, 18.45, '23%'),
       ('PR14', 'P1', 'Bluza', 'Bluza męska', 195.00, 239.85, 240.00, 295.20, '23%'),
       ('PR15', 'P2', 'Kąpielówki', 'Kąpielówki dla mężczyzn', 89.00, 109.47, 139.00, 170.97, '23%'),
       ('PR16', 'P2', 'Kostium kąpielowy', 'Bikini dla kobiet', 129.00, 158.67, 159.00, 195.57, '23%'),
       ('PR17', 'P2', 'Kostium kąpielowy jednoczęściowy', 'Kostium dla kobiet', 149.00, 183.27, 189.00, 232.47, '23%'),
       ('PR18', 'P1', 'Bluza z kapturem', 'Bluza damska', 188.90, 232.35, 249.00, 306.27, '23%'),
       ('PR19', 'P2', 'Spodnie 3/4', 'Rybaczki dla mężczyzn', 135.00, 166.05, 170.00, 209.10, '23%'),
       ('PR20', 'P3', 'Koszulka sportowa', 'Koszulka do ćwiczeń', 79.00, 97.17, 119.00, 146.37, '23%');
       
-- Tabela "Zamówienia" - 10 pozycji

USE sklep_odziezowy;

INSERT INTO zamowienia
VALUES ('Z1', 'K1', 'PR2', '2022-01-15'),
	   ('Z2', 'K2', 'PR17', '2022-01-15'),
       ('Z3', 'K7', 'PR3', '2021-12-17'),
       ('Z4', 'K9', 'PR3', '2022-07-12'),
       ('Z5', 'K5', 'PR4', '2022-08-11'),
       ('Z6', 'K6', 'PR15', '2022-05-14'),
       ('Z7', 'K3', 'PR19', '2021-12-31'),
       ('Z8', 'K4', 'PR1', '2022-02-22'),
       ('Z9', 'K8', 'PR8', '2022-04-01'),
       ('Z10', 'K10', 'PR7', '2021-09-13');
       
-- Tabela "Klienci" - 10 pozycji

INSERT INTO klienci
VALUES ('K1', 'Z1', 'Adam', 'Nowak', 'ul. Banacha 2/11, Warszawa'),
	   ('K2', 'Z2', 'Monika', 'Kowalska', 'ul. Jana 3/4, Toruń'),
       ('K3', 'Z3', 'Aleksandra', 'Piękna', 'ul. Głowackiego 23/5, Bydgoszcz'),
       ('K4', 'Z8', 'Miłosz', 'Kopański', 'ul. Nowa 5/78, Katowice'),
       ('K5', 'Z5', 'Mateusz', 'Kwiatkowski', 'ul. Miła 45/7, Gdańsk'),
       ('K6', 'Z6', 'Agnieszka', 'Nowacka', 'ul. Łódzka 23/4, Zgierz'),
       ('K7', 'Z3', 'Dawid', 'Misiak', 'ul. Toruńska 24, Tczew'),
       ('K8', 'Z9', 'Małgorzata', 'Palińska', 'ul. Nastrojowa 45, Gdynia'),
       ('K9', 'Z4', 'Łukasz', 'Panasiuk', 'ul. Przedwiośnie 4, Zabrze'),
       ('K10', 'Z10', 'Olgierd', 'Łukawski', 'ul. Wybrzeża 23, Grodzisk Mazowiecki');

-- ad 8 - Wyświetl wszystkie produkty ze wszystkimi danymi od producenta, który znajduje się na pozycji 1 w tabeli "Producenci" 

USE sklep_odziezowy;

SELECT *
FROM produkty
INNER JOIN producenci
ON produkty.id_producenta=producenci.id_producenta
WHERE produkty.id_producenta='P1' AND producenci.id_producenta='P1';

-- ad 9 - Posortuj te produkty alfabetycznie po nazwie 

SELECT *
FROM produkty
INNER JOIN producenci
ON produkty.id_producenta=producenci.id_producenta
WHERE produkty.id_producenta='P1' AND producenci.id_producenta='P1'
ORDER BY nazwa_produktu ASC;

-- ad 10 - Wylicz średnią cenę za produkt od producenta z pozycji 1 - wyliczam średnią cenę brutto sprzedaży 

SELECT ROUND(AVG(produkty.cena_brutto_sprzedazy),2)
FROM produkty
WHERE produkty.id_producenta='P1';

-- ad 11 - Wyświetl dwie grupy produktów tego producenta
-- połowa najtańszych to grupa: "Tanie" - cenie sprzedaży brutto -- pozostałe to grupa: "Drogie"

SELECT cena_brutto_sprzedazy
FROM produkty
WHERE id_producenta='P1'
ORDER BY cena_brutto_sprzedazy ASC

SELECT nazwa_produktu, cena_brutto_sprzedazy,
CASE
     WHEN cena_brutto_sprzedazy <= 147.60 THEN 'Tanie'
     WHEN cena_brutto_sprzedazy> 147.60 THEN 'Drogie'
     ELSE 'Produkty producenta P1'
END AS Typologia
FROM produkty
WHERE id_producenta='P1'
ORDER BY cena_brutto_sprzedazy ASC

-- ad 12 - Wyświetl produkty zamówione, wyświetlając tylko ich nazwe 

SELECT produkty.nazwa_produktu
FROM produkty
JOIN zamowienia
ON produkty.id_produktu=zamowienia.id_produktu;

-- ad 13 - Wyświetl wszystkie produkty zamówione - ograniczając wyświetlanie do 5 pozycji 

SELECT produkty.nazwa_produktu, zamowienia.id_zamowienia
FROM produkty
JOIN zamowienia
ON produkty.id_produktu=zamowienia.id_produktu
LIMIT 5

-- ad 14 - Policz łączną wartość wszystkich zamówień - liczę po cenie brutto sprzedaży

USE sklep_odziezowy;

SELECT SUM(cena_brutto_sprzedazy)
FROM produkty;

-- ad 15 - Wyświetl wszystkie zamówienia wraz z nazwą produktu sortujac je wg daty od najstarszego do najnowszego -- wyświetlam z id_zamowienia i id_produktu 

SELECT produkty.nazwa_produktu, zamowienia.id_zamowienia, zamowienia.id_produktu, zamowienia.data_zamowienia
FROM produkty
JOIN zamowienia
ON produkty.id_produktu=zamowienia.id_produktu
ORDER BY data_zamowienia ASC

-- ale może też być tylko id_zamowienia bez id_produktu

SELECT produkty.nazwa_produktu, zamowienia.id_zamowienia, zamowienia.data_zamowienia
FROM produkty
JOIN zamowienia
ON produkty.id_produktu=zamowienia.id_produktu
ORDER BY data_zamowienia ASC

-- ad 16 - Sprawdz, czy w tabeli produkty masz uzupełnione wszystkie dane - wyświetl pozycje, dla których brakuje danych -- Uwaga! - mam uzupełnione w tabeli wszystkie dane

SELECT*
FROM produkty;

-- Sprawdzam wszystkie warości, które nie są NULL

SELECT*
FROM produkty
WHERE id_produktu IS NOT NULL 
OR id_producenta IS NOT NULL 
OR nazwa_produktu IS NOT NULL 
OR opis_produktu IS NOT NULL 
OR cena_netto_zakupu IS NOT NULL 
OR cena_brutto_zakupu IS NOT NULL 
OR cena_netto_sprzedazy IS NOT NULL 
OR cena_brutto_sprzedazy IS NOT NULL 
OR procent_VAT_sprzedazy IS NOT NULL;

-- Sprawdzam wartości, które są NULL

SELECT*
FROM produkty
WHERE id_produktu IS NULL 
OR id_producenta IS NULL 
OR nazwa_produktu IS NULL 
OR opis_produktu IS NULL 
OR cena_netto_zakupu IS NULL 
OR cena_brutto_zakupu IS NULL 
OR cena_netto_sprzedazy IS NULL 
OR cena_brutto_sprzedazy IS NULL 
OR procent_VAT_sprzedazy IS NULL;

-- 17 - Wyświetl produkt najczęściej sprzedawany wraz z jego ceną

SELECT P.nazwa_produktu, COUNT(P.nazwa_produktu), P.cena_brutto_sprzedazy
FROM zamowienia Z
LEFT JOIN produkty P
ON Z.id_produktu=P.id_produktu
GROUP BY P.nazwa_produktu
ORDER BY COUNT(P.nazwa_produktu) DESC
LIMIT 1

-- 18 Znajdz dzień, w którym najwięcej zostało złożonych zamówień

SELECT COUNT(data_zamowienia), data_zamowienia
FROM zamowienia
GROUP BY data_zamowienia
ORDER BY COUNT(data_zamowienia) DESC
LIMIT 1


