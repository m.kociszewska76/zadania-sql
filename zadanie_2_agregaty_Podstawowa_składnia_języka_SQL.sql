-- Zadanie 2 Agregaty Podstawowa składnia języka SQL

-- ad a
USE aggregates;

SELECT first_name, last_name, 
CONCAT(first_name, " ", last_name) AS Pracownik
FROM batman;

-- ad b 

SELECT CONCAT(gift, ' - ', price) AS 'Cennik' 
FROM batman;

-- ad c -- zmieniłam alias dla tej kolumny z "Cennik" na "Cennik + imię, nazwisko"

SELECT CONCAT(gift, ' - ', price, ' ',first_name,' ', last_name) AS 'Cennik + imię, nazwisko' 
FROM batman;

-- ad d

SELECT 
  CONCAT(UPPER(SUBSTRING(gift,1,1)),UPPER(SUBSTRING(gift,2, LENGTH(gift)))) AS Prezenty
FROM batman;

-- ad e

SELECT 
  CONCAT(LOWER(SUBSTRING(first_name,1,1)), LOWER(SUBSTRING(first_name,2, LENGTH(first_name)))) AS imiona
FROM batman;

-- ad f 

SELECT last_name,
LENGTH(last_name) AS Dlugosc
FROM batman;

-- ad g 

SELECT first_name,
SUBSTRING(first_name,1,2) 
FROM batman;

-- ad h 

SELECT first_name, last_name,
CONCAT(LOWER(SUBSTRING(first_name,1)), LOWER(SUBSTRING(last_name,1,3))) AS 'login uzytkownika'
FROM batman;
