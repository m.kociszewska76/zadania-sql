-- Zadanie 4 Agregaty - Podstawowa składnia jezyka SQL

-- ad a
SELECT ROUND (AVG(age), 2)
FROM aggregates.batman;

-- ad b
SELECT ABS(-3);

-- ad c
SELECT 2*3;

-- ad d
SELECT 6/2;